import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { App } from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  newsList: any;
  searchQuery: string = '';
  items: string[];
  constructor(
    public navCtrl: NavController,
    public httpClient: HttpClient,
    public alertCtrl: AlertController
  
  ) {
    this.initializeItems();
  }

  initializeItems() {
    this.items = [
    ];
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  ionViewDidLoad() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
      };
    this.httpClient.post(App.apiUrl + 'getNewsList', {language: App.language, rootId: 'NEWS_ROOT'}, {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        this.newsList = res["newsList"];
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  get uiLabelMap() {
    return App.uiLabelMap;
  }
}
