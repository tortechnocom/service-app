import { Component } from '@angular/core';
import {App} from '../../app/app.component';
import {NavController, AlertController, LoadingController, ToastController} from 'ionic-angular';
import { ForgotSetPage } from '../forgotset/forgot.set';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'forgot-page',
  templateUrl: 'forgot.html'
})
export class ForgotPage {
  private input: any = {
    email: null
  };
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    public httpClient: HttpClient,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
    ) {
  }
  sendCodeEmail() {
    let loader = this.loadingCtrl.create({
      content: "Sending..."
    });
    loader.present();
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
      };
    this.httpClient.post(App.apiUrl + 'forgotPassword', JSON.stringify(this.input), {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      loader.dismiss();
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        let toast = this.toastCtrl.create({
          message: res['successMessage'],
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        this.nav.push(ForgotSetPage, {email: this.input.email});
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  get uiLabelMap() {
    return App.uiLabelMap;
  }

}
