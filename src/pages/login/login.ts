import { Component } from '@angular/core';
import { App } from '../../app/app.component';
import { NavController, AlertController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { ForgotPage } from '../forgot/forgot';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HomePage } from '../home/home';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';


@Component({
  selector: 'login-page',
  templateUrl: 'login.html'
})
export class LoginPage {
  private input: any = {
    email: "",
    password: ""
  };
  private rememberMe = false;
  constructor(
    public nav: NavController,
    public httpClient: HttpClient,
    public alertCtrl: AlertController,
    private fb: Facebook,
    private googlePlus: GooglePlus
    ) {
      if (localStorage.getItem("rememberMe")) {
        this.rememberMe = (localStorage.getItem("rememberMe") == 'true');
      }
      if (this.rememberMe == true) {
        this.input = {
          email: localStorage.getItem("email"),
          password: localStorage.getItem("password")
        }
      }
      

  }
  get uiLabelMap() {
    return App.uiLabelMap;
  }
  login() {
    return new Promise((resolve, reject) => {
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
        };
      this.httpClient.post(App.apiUrl + 'login', JSON.stringify(this.input), {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          App.setUser(res['authToken']);
          let headerJson = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': App.authToken
            };
            this.httpClient.post(App.apiUrl + 'getAccount', {}, {
              headers: new HttpHeaders(headerJson)
            })
            .subscribe(res => {
              let title = "Response";
              if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
                let alert = this.alertCtrl.create({
                  title: title,
                  subTitle: res['errorMessage'],
                  buttons: ['OK']
                });
                alert.present();
              } else {
                App.user = res['user'];
                App.refreshMenu();
                this.nav.setRoot(HomePage);
                this.nav.popToRoot();
              }
              //resolve(res);
            }, (err) => {
              console.log('err: ', err);
              //reject(err);
            });
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
    });
  }
  goToRegister() {
    this.nav.push(RegisterPage);
  }
  goToForgot() {
    this.nav.push(ForgotPage);
  }
  loginWithFacebook() {
    this.fb.login(['public_profile', 'email'])
      .then((res1: FacebookLoginResponse) => {
        console.log('Logged into Facebook!', res1);
        let url = '/me?fields=name,email';
        this.fb.api(url, ["name", "email"]).then((res2: FacebookLoginResponse) => {
          let authResponse = res1["authResponse"];
          console.log('API Response', res2);
          let headerJson = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
            };
          let name = res2["name"].split(" ");
          let firstName = "";
          let lastName = "";
          if (name.length > 1) {
            firstName = name[0];
            lastName = name[1];
          } else if (name.length == 1) {
            firstName = name[0];
          }
          let data = {email: res2["email"],
            firstName: firstName,
            lastName: lastName,
            authToken: authResponse["accessToken"],
            expiresIn: authResponse["expiresIn"],
            facebookId: authResponse["userID"]
          };
          console.log("data: ", data);
          this.httpClient.post(App.apiUrl + 'loginWithFacebook', JSON.stringify(data), {
            headers: new HttpHeaders(headerJson)
          })
          .subscribe(res => {
            let title = "Response";
            if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
              let alert = this.alertCtrl.create({
                title: title,
                subTitle: res['errorMessage'],
                buttons: ['OK']
              });
              alert.present();
            } else {
              App.setUser(res['authToken']);
              let headerJson = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': App.authToken
                };
                this.httpClient.post(App.apiUrl + 'getAccount', {}, {
                  headers: new HttpHeaders(headerJson)
                })
                .subscribe(res => {
                  let title = "Response";
                  if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
                    let alert = this.alertCtrl.create({
                      title: title,
                      subTitle: res['errorMessage'],
                      buttons: ['OK']
                    });
                    alert.present();
                  } else {
                    App.user = res['user'];
                    App.refreshMenu();
                    this.nav.setRoot(HomePage);
                    this.nav.popToRoot();
                  }
                  //resolve(res);
                }, (err) => {
                  console.log('err: ', err);
                  //reject(err);
                });
            }
            //resolve(res);
          }, (err) => {
            console.log('err: ', err);
            //reject(err);
          });
        });
      })
      .catch(e => console.log('Error logging into Facebook', e));
    }
    loginWithGooglePlus() {
      this.googlePlus.login({})
        .then(res => {
          let headerJson = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
            };
          let data = {
            email: res["email"],
            firstName: res["givenName"],
            lastName: res["familyName"],
            authToken: res["accessToken"],
            expires: res["expires"],
            googleId: res["userId"]
          };
          console.log("data: ", data);
          this.httpClient.post(App.apiUrl + 'loginWithGooglePlus', JSON.stringify(data), {
            headers: new HttpHeaders(headerJson)
          })
          .subscribe(res => {
            let title = "Response";
            if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
              let alert = this.alertCtrl.create({
                title: title,
                subTitle: res['errorMessage'],
                buttons: ['OK']
              });
              alert.present();
            } else {
              App.setUser(res['authToken']);
              let headerJson = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': App.authToken
                };
                this.httpClient.post(App.apiUrl + 'getAccount', {}, {
                  headers: new HttpHeaders(headerJson)
                })
                .subscribe(res => {
                  let title = "Response";
                  if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
                    let alert = this.alertCtrl.create({
                      title: title,
                      subTitle: res['errorMessage'],
                      buttons: ['OK']
                    });
                    alert.present();
                  } else {
                    App.user = res['user'];
                    App.refreshMenu();
                    this.nav.setRoot(HomePage);
                    this.nav.popToRoot();
                  }
                  //resolve(res);
                }, (err) => {
                  console.log('err: ', err);
                  //reject(err);
                });
            }
            //resolve(res);
          }, (err) => {
            console.log('err: ', err);
            //reject(err);
          });
        })
        .catch(err => console.error(err));
    }
}
