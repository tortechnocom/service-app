import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {App} from '../../app/app.component';
import {NavController, AlertController, NavParams} from 'ionic-angular';

@Component({
  selector: 'register-start-page',
  templateUrl: 'register-start.html'
})
export class RegisterStartPage {
  private input: any = {
    email: null,
    currentPassword: null,
    currentPasswordVerify: null
  };
  constructor(
    public nav: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public httpClient: HttpClient
    ) {
      this.input.email = this.navParams.get("email");
  }
  startAccount() {
    return new Promise((resolve, reject) => {
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
        };
      this.httpClient.post(App.apiUrl + 'createAccount', JSON.stringify(this.input), {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        console.log('res: ', res);
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['successMessage'],
            buttons: ['OK']
          });
          alert.present();
          this.nav.popToRoot();
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
    });
  }
  get uiLabelMap() {
    return App.uiLabelMap;
  }

}