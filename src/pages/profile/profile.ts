import { Component } from '@angular/core';
import {App} from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, NavController } from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
  selector: 'profile-page',
  templateUrl: 'profile.html'
})

export class ProfilePage {
    showPassword = false;
    constructor(
        public httpClient: HttpClient,
        public alertCtrl: AlertController,
        public nav: NavController,
    ) {

    }
    signOut() {
        return new Promise((resolve, reject) => {
          let headerJson = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': App.authToken
            };
          this.httpClient.post(App.apiUrl + 'logout', null, {
            headers: new HttpHeaders(headerJson)
          })
          .subscribe(res => {
            let title = "Response";
            if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
              let alert = this.alertCtrl.create({
                title: title,
                subTitle: res['errorMessage'],
                buttons: ['OK']
              });
              alert.present();
            } else {
              App.clearAuthToken();
              App.refreshMenu();
              this.nav.setRoot(HomePage); 
              this.nav.popToRoot();
            }
            //resolve(res);
          }, (err) => {
            console.log('err: ', err);
            //reject(err);
          });
        });
      }
      get uiLabelMap() {
        return App.uiLabelMap;
      }
      get user() {
        return App.user;
      }
}