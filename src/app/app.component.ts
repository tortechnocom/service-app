import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: "app",
  templateUrl: 'app.html'
})
export class App {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  static user: any;
  public static language: any;
  languageList = [
    {language: "th", name: "Thai", selected: "selected"},
    {language: "au", name : "English", selected: ""}]
    // APIs
  public static apiUrl:string = "https://service-backend.mymemoapps.com/api/";
  public static authToken:string;
  public static pages: Array<{id: string,title: string, component: any, visible: boolean, icon: string}>;
  public static uiLabelMap: any;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public menu: MenuController,
    public alertCtrl: AlertController,
    public httpClient: HttpClient
  ) {
    this.setLanguage(localStorage.getItem("language"));
  }

  initializeApp() {
    App.pages = [
      { id: "HOME", title: App.uiLabelMap.APIHome, component: HomePage, visible: true, icon: "home" },
      { id: "ACCOUNT", title: App.uiLabelMap.APIAccount, component: ProfilePage, visible: false, icon: "person"},
      { id: "SIGN_IN", title: App.uiLabelMap.APISignIn, component: LoginPage, visible: true, icon: "lock"}
    ];
    App.setUser(null);
    if (App.authToken) {
      this.getUser();
    }
    App.refreshMenu();

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  setLanguage(value) {
    if (value ==  null) value = "au";
    if (value == "au") {
      App.language = "en";
    } else {
      App.language = value
    }
    this.getUiLabels();
    localStorage.setItem("language", value);
    let count = 0;
    for(let lang  of this.languageList) {
      if (lang.language == value) {
        this.languageList[count].selected = "selected";
      } else {
        this.languageList[count].selected = "";
      }
      count++;
    }
    if (this.nav) {
      if ("HomePage" == this.nav.root.name) {
        this.nav.goToRoot({});
      }
    }
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    //this.rootPage = page.component;
    this.nav.root = page.component
    this.menu.close();
  }
  public static refreshMenu() {
    var page;
    if (App.authToken == null) { // Logged out
      for (page of App.pages) {
        if (page.id == "SIGN_IN" || page.id == "HOME") {
          page.visible = true;
        } else {
          page.visible = false;
        }
      }
    } else { // Logged in
      for (page of this.pages) {
        if (page.id == "SIGN_IN") {
          page.visible = false;
        } else {
          page.visible = true;
        }
      }
    }
  }
  static setUser(authToken) {
    if (authToken == null && localStorage.getItem("authToken") != null) {
      authToken = localStorage.getItem("authToken");
    } else if (authToken != null) {
      localStorage.setItem('authToken', authToken);
    }
    App.authToken = authToken;
  }
  static getAuthToken(authToken) {
        App.authToken;
  }
  static clearAuthToken () {
    App.authToken = null;
    sessionStorage.removeItem('authToken');
  }
  static getPages () {
    return this.pages;
  }
  get pages() {
    return App.pages;
  }
  get uiLabelMap () {
    return App.uiLabelMap;
  }
  getUiLabels() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
      };
    this.httpClient.post(App.apiUrl + 'getUiLabels', {language: App.language}, {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        App.uiLabelMap = res["uiLabelMap"];
        this.initializeApp();
        this.rootPage = HomePage;
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  getUser() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
      this.httpClient.post(App.apiUrl + 'getAccount', {}, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          localStorage.removeItem("authToken");
          this.rootPage = LoginPage;
        } else {
          App.user = res['user'];
          this.rootPage = HomePage;
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
}
