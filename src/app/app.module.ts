import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { App } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { RegisterPage } from '../pages/register/register';
import { ForgotPage } from '../pages/forgot/forgot';
import { ForgotSetPage } from '../pages/forgotset/forgot.set';
import { RegisterStartPage } from '../pages/register-start/register-start';
import { HttpClientModule } from '@angular/common/http';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';

@NgModule({
  declarations: [
    App,
    HomePage,
    LoginPage,
    ProfilePage,
    RegisterPage,
    ForgotPage,
    ForgotSetPage,
    RegisterStartPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(App),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    App,
    HomePage,
    LoginPage,
    ProfilePage,
    RegisterPage,
    ForgotPage,
    ForgotSetPage,
    RegisterStartPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Facebook,
    GooglePlus,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
